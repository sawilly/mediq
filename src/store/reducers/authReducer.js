import * as types from './../actions/actionTypes';
import initialState from './initialState';

export default function authReducer(state = initialState.isAuthenticated, action) {
    switch(action.type) {
        case types.TOKEN:
            return state;
        case types.SIGNOUT:
            state = false;
            return state;
        default:
            return state;
    }
}