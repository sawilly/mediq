import {combineReducers} from 'redux';
import authentication  from './authReducer';
import user from './userReducer';
import patients from './patientsRedcer';

const rootReducer = combineReducers({
    auth: authentication,
    user: user,
    patients: patients
});

export default rootReducer;