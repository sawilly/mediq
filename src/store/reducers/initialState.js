export default {
    token: localStorage.getItem('access_token') || null,
    isAuthenticated: localStorage.getItem('access_token') ? true : false,
    user: {},
    patients: [],
    procedures: [],
    notes: [],
    billings: []
}