import * as types from './actionTypes';

import axios from 'axios';
axios.defaults.baseURL = "http://localhost:8000/api";

export function loadPatients(patients) {
    return { type: types.PATIENTS, patients };
}

export function loadAllPatients() {
    return function(dispatch) {
        axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');
        return axios.get('/patients').then(response => {
            dispatch(loadPatients(response.data));
        }).catch(error => {
            throw(error);
        });
    };
}