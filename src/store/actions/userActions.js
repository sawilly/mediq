import * as types from './actionTypes';

import axios from 'axios';
axios.defaults.baseURL = "http://localhost:8000/api";

export function loadUserDetails(user) {
    return { type: types.USER, user };
}

export function loadDetails() {
    return function(dispatch) {
        axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');
        return axios.get('/user').then(response => {
            dispatch(loadUserDetails(response.data));
        }).catch(error => {
            throw(error);
        });
    };
}