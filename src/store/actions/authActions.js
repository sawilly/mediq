import * as types from './actionTypes';

import axios from 'axios';
axios.defaults.baseURL = "http://localhost:8000/api";

export function accessToken(token) {
    return { type: types.TOKEN, token}
}

export function isAuthenticatedStatus(authenticated) {
    return { type: types.SIGNOUT,  authenticated}
}

export function signin(credentials) {
    return function(dispatch) {
        return axios.post('/login', { username: credentials.username, password: credentials.password }).then(response => {
            const token  = response.data.access_token;
            localStorage.setItem('access_token', token);
            dispatch(accessToken(token));
        })
        .catch(error => {
            // console.log(error.response.data);
            throw(error.response.data);
        })
    }
}

export function signout() {
    return function(dispatch) {
        axios.defaults.headers.common['Authorization'] = 'Bearer '+ localStorage.getItem('access_token');
        return axios.post('/logout').then(reponse => {
            localStorage.removeItem('access_token');
            const authenticated = false;
            dispatch(isAuthenticatedStatus(authenticated));
        })
        .catch(error => {
            console.log(error);
            throw(error);
        })
    }
}