import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import Layout from './../layout/layout';

import { LineChart, Line, ResponsiveContainer, BarChart, Bar, PieChart, Pie, Legend, Tooltip, Cell, AreaChart, Area, XAxis, YAxis, CartesianGrid } from 'recharts';

import { Card, Grid, Statistic } from 'semantic-ui-react';

import BlockLoader from './../loaders/block';

import TableLoader from './../loaders/table';

import * as userActions from './../../store/actions/userActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

const data = [
    {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
    {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
    {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
    {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
    {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
    {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
    {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
];

const pie_data = [
    {name: 'Group A', value: 2400}, 
    {name: 'Group B', value: 4567},
    {name: 'Group C', value: 1398}, 
    {name: 'Group D', value: 9800},
    {name: 'Group E', value: 3908}, 
    {name: 'Group F', value: 4800}
];

const COLORS = ['#38d21a','#179af8','#64dd17','#1565c0','#76ff03','#1e88e5','#38d21a','#90caf9',];

class Dashboard extends Component {

    state = {
        loading: true
    }

    componentDidMount() {
        this.props.actions.loadDetails();
        setTimeout(() => this.setState({ loading: false }), 1500); // simulates an async action, and hides the spinner
    }

    render() {

        const {loading} = this.state;
        const {user} = this.props;

        return (
            <div>
                <Layout>
                    <Grid stackable>
                        <Grid.Row columns={1}>
                            <Grid.Column>
                                {loading && <TableLoader width="25%" height="36px" />}
                                {!loading &&
                                    <div><h1>Welcome Dr. {user.firstname} {user.middlename} {user.lastname}</h1></div>
                                }
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={3}>
                        <Grid.Column>
                                <Card fluid>
                                    <Card.Content>
                                        <Card.Header>Patients</Card.Header>
                                        <Card.Description>No. Of Patients Treated This Year</Card.Description>
                                    </Card.Content>
                                    <Card.Content extra className="card-content">
                                        {loading && <BlockLoader height="200px" />}
                                        {!loading &&
                                        <ResponsiveContainer width="100%" height={200}>
                                            <LineChart data={data}>
                                                <Line type="monotone" dataKey="uv" stroke="#179af8" />
                                                <Line type="monotone" dataKey="pv" stroke="#38d21a" />
                                                <Legend />
                                                <Tooltip/>
                                            </LineChart>
                                        </ResponsiveContainer>
                                        }
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                            <Grid.Column>
                                <Card fluid>
                                    <Card.Content>
                                        <Card.Header>Billing Statistics</Card.Header>
                                        <Card.Description>No. of Total Paid Amounts Against Unpaid Amount</Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        {loading && <BlockLoader height="200px" />}
                                        {!loading &&
                                        <ResponsiveContainer width="100%" height={200}>
                                            <BarChart data={data}>
                                                <Bar dataKey='uv' fill='#179af8'/>
                                                <Bar dataKey='pv' fill='#38d21a'/>
                                                <Legend />
                                                <Tooltip/>
                                            </BarChart>
                                        </ResponsiveContainer>
                                        }
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                            <Grid.Column>
                                <Card fluid>
                                    <Card.Content>
                                        <Card.Header>General Statistics</Card.Header>
                                        <Card.Description>Procedures, Notes And Billing Totals.</Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        {loading && <BlockLoader height="200px" />}
                                        {!loading &&
                                        <ResponsiveContainer width="100%" height={200}>
                                            <PieChart>
                                                <Pie dataKey="value" data={pie_data} innerRadius={40} outerRadius={60} fill="#179af8">
                                                {
                                                    pie_data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
                                                }
                                                </Pie>
                                                <Legend />
                                                <Tooltip/>
                                            </PieChart>
                                        </ResponsiveContainer>
                                        }
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={4}   >
                            <Grid.Column>
                                <Card fluid as={Link} to="/patients"> 
                                    <Card.Content>
                                        <Statistic className="statistics">
                                            <Statistic.Value className="green">0</Statistic.Value>
                                            <Statistic.Label className="blue">Patients</Statistic.Label>
                                        </Statistic>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                            <Grid.Column>
                                <Card fluid as={Link} to="/procedures">
                                    <Card.Content>
                                        <Statistic className="statistics">
                                            <Statistic.Value className="green">0</Statistic.Value>
                                            <Statistic.Label className="blue">Procedures</Statistic.Label>
                                        </Statistic>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                            <Grid.Column>
                                <Card fluid as={Link} to="/notes">
                                    <Card.Content>
                                        <Statistic className="statistics">
                                            <Statistic.Value className="green">0</Statistic.Value>
                                            <Statistic.Label className="blue">Medical Notes</Statistic.Label>
                                        </Statistic>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                            <Grid.Column>
                                <Card fluid as={Link} to="/billings">
                                    <Card.Content>
                                        <Statistic className="statistics">
                                            <Statistic.Value className="green">0</Statistic.Value>
                                            <Statistic.Label className="blue">Billings</Statistic.Label>
                                        </Statistic>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={1}>
                            <Grid.Column>
                                <Card fluid>
                                    <Card.Content>
                                        <Card.Header>Summary</Card.Header>
                                        <Card.Description>Distribution Chart For General System Statistics</Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        {loading && <BlockLoader height="200px" />}
                                        {!loading &&
                                        <ResponsiveContainer width="100%" height={200}>
                                            <AreaChart data={data} margin={{top: 10, right: 30, left: 0, bottom: 0}}>
                                                <CartesianGrid strokeDasharray="3 3"/>
                                                <XAxis dataKey="name"/>
                                                <YAxis/>
                                                <Tooltip/>
                                                <Area type='monotone' dataKey='uv' stackId="1" stroke='#179af8' fill='#179af8' />
                                                <Area type='monotone' dataKey='pv' stackId="1" stroke='#38d21a' fill='#38d21a' />
                                                <Area type='monotone' dataKey='amt' stackId="1" stroke='#90caf9' fill='#90caf9' />
                                            </AreaChart>
                                        </ResponsiveContainer>
                                        }
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    
                </Layout>
            </div>
        );
    }

}

Dashboard.propTypes = {
    dispatch: PropTypes.func,
    actions: PropTypes.object.isRequired
}
  
const mapStateToProps = (state, ownProps) => {
    return {
      user: state.user
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);