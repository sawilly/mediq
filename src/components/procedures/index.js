import React, { Component } from 'react';
import Layout from './../layout/layout';
import { Grid, Table, Dropdown, Modal, Icon, Button, Form, Header } from 'semantic-ui-react';
import TableLoader from './../loaders/table';

const patients = [
  { key: 1, text: 'Michael Joseph', value: 1 },
  { key: 2, text: 'Nia Guzman', value: 2 },
  { key: 3, text: 'Abraham Lincoln', value: 3 },
  { key: 4, text: 'Dwayne Johnson', value: 4 },
];

export default class procedures extends Component {

  //Initial States For Procedure Component
  state = {
      loading: true,
      open: false, 
      size: 'large',
      openEditModal: false,
      sizeEditModal: 'large',
      openCloseModal: false,
      sizeCloseModal: 'small'
  }

  //Add Procedure Modal Window Functions
  show = size => () => this.setState({ size, open: true })
  close = () => this.setState({ open: false })

  //Edit Procedure Modal Window Functions
  showEditModal = size => () => this.setState({ sizeEditModal: size, openEditModal: true })
  closeEditModal = () => this.setState({ openEditModal: false })

  //Close Procedure Modal Window Functions
  showCloseModal = size => () => this.setState({ sizeCloseModal: size, openCloseModal: true })
  closeCloseModal = () => this.setState({ openCloseModal: false })

  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 1500); // simulates an async action, and hides the spinner
  }

  render() {

    const {size, open, loading, openEditModal, sizeEditModal, openCloseModal, sizeCloseModal} = this.state;

    return (
      <div>
        <Layout>
            <div className="page-wrapper">
              <Grid stackable>
                <Grid.Row columns={1}>
                  <Grid.Column>
                    <div className="flex-box">
                      <div>
                        {loading && <TableLoader width="144px" height="36px" />}
                        {!loading &&
                          <h1>Procedures</h1>
                        }
                      </div>
                      <div>
                        {loading && <TableLoader width="158px" height="36px" />}
                        {!loading &&
                          <Button color='green' className="btn" onClick={this.show('large')}>
                              <Icon name='plus' /> Add Procedure
                          </Button>
                        }
                      </div>
                    </div>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={1}>
                  <Grid.Column>
                    {loading && <TableLoader width="100%" height="41px" loop={5}/>}
                    {!loading && 
                    <Table basic='very'>
                      <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Date</Table.HeaderCell>
                            <Table.HeaderCell>Procedure</Table.HeaderCell>
                            <Table.HeaderCell>Description</Table.HeaderCell>
                            <Table.HeaderCell>Patient</Table.HeaderCell>
                            <Table.HeaderCell>Action</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                        <Table.Body>
                          <Table.Row>
                            <Table.Cell>September 14, 2013</Table.Cell>
                            <Table.Cell>Tenetur</Table.Cell>
                            <Table.Cell>Facilis qui nisi voluptatem earum praesentium illu...</Table.Cell>
                            <Table.Cell>John Lilki</Table.Cell>
                            <Table.Cell>
                              <Dropdown text='Actions'>
                                <Dropdown.Menu>
                                    <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                    <Dropdown.Item text='View' />
                                    <Dropdown.Divider />
                                    <Dropdown.Item text='Delete' onClick={this.showCloseModal('small')}/>
                                  </Dropdown.Menu>
                              </Dropdown>
                            </Table.Cell>
                          </Table.Row>
                          <Table.Row>
                            <Table.Cell>September 14, 2013</Table.Cell>
                            <Table.Cell>Tenetur</Table.Cell>
                            <Table.Cell>Facilis qui nisi voluptatem earum praesentium illu...</Table.Cell>
                            <Table.Cell>John Lilki</Table.Cell>
                            <Table.Cell>
                              <Dropdown text='Actions'>
                                <Dropdown.Menu>
                                    <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                    <Dropdown.Item text='View' />
                                    <Dropdown.Divider />
                                    <Dropdown.Item text='Delete' onClick={this.showCloseModal('small')}/>
                                  </Dropdown.Menu>
                              </Dropdown>
                            </Table.Cell>
                          </Table.Row>
                          <Table.Row>
                            <Table.Cell>September 14, 2013</Table.Cell>
                            <Table.Cell>Tenetur</Table.Cell>
                            <Table.Cell>Facilis qui nisi voluptatem earum praesentium illu...</Table.Cell>
                            <Table.Cell>John Lilki</Table.Cell>
                            <Table.Cell>
                              <Dropdown text='Actions'>
                                <Dropdown.Menu>
                                    <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                    <Dropdown.Item text='View' />
                                    <Dropdown.Divider />
                                    <Dropdown.Item text='Delete' onClick={this.showCloseModal('small')}/>
                                  </Dropdown.Menu>
                              </Dropdown>
                            </Table.Cell>
                          </Table.Row>
                          <Table.Row>
                            <Table.Cell>September 14, 2013</Table.Cell>
                            <Table.Cell>Tenetur</Table.Cell>
                            <Table.Cell>Facilis qui nisi voluptatem earum praesentium illu...</Table.Cell>
                            <Table.Cell>John Lilki</Table.Cell>
                            <Table.Cell>
                              <Dropdown text='Actions'>
                                <Dropdown.Menu>
                                    <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                    <Dropdown.Item text='View' />
                                    <Dropdown.Divider />
                                    <Dropdown.Item text='Delete' onClick={this.showCloseModal('small')}/>
                                  </Dropdown.Menu>
                              </Dropdown>
                            </Table.Cell>
                          </Table.Row>
                        </Table.Body>
                    </Table>
                    }
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
        </Layout>
        {/* Modal Window For Adding Procedure */}
        <Modal size={size} open={open} onClose={this.close} closeIcon>
          <Modal.Header>Add Procedure</Modal.Header>
            <Modal.Content>
              <Form>
                <Form.Group widths='equal'>
                  <Form.Select fluid label='Patient' options={patients} placeholder='Patient' />
                </Form.Group>
                <Form.Group widths='equal'>
                  <Form.Input fluid label='Procedure' placeholder='Procedure' />
                </Form.Group>
                <Form.Group widths='equal'>
                  <Form.TextArea label='Description' placeholder='Write ...' />
                </Form.Group>
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button color='red' onClick={this.close}>
                <Icon name='remove' /> Cancel
              </Button>
                <Button color='green' className="btn" onClick={this.close}>
                  <Icon name='save' /> Add Procedure
                </Button>
            </Modal.Actions>
        </Modal>
        {/* Modal Window For Editing Procedure */}
        <Modal size={sizeEditModal} open={openEditModal} onClose={this.closeEditModal} closeIcon>
          <Modal.Header>Edit Procedure</Modal.Header>
            <Modal.Content>
              <Form>
                <Form.Group widths='equal'>
                  <Form.Select fluid label='Patient' options={patients} placeholder='Patient' />
                </Form.Group>
                <Form.Group widths='equal'>
                  <Form.Input fluid label='Procedure' placeholder='Procedure' />
                </Form.Group>
                <Form.Group widths='equal'>
                  <Form.TextArea label='Description' placeholder='Write ...' />
                </Form.Group>
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button color='red' onClick={this.closeEditModal}>
                <Icon name='remove' /> Cancel
              </Button>
                <Button primary onClick={this.closeEditModal}>
                  <Icon name='save' /> Edit Procedure
                </Button>
            </Modal.Actions>
        </Modal>
        {/* Delete Patient Modal Window */}
        <Modal basic size={sizeCloseModal} open={openCloseModal} onClose={this.closeCloseModal} closeIcon>
          <Header icon='trash alternate' content='Delete Procedure Details' />
          <Modal.Content>
            <p>
              Are You Sure You Want To Delete The Procedure Details ?
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button color='red' inverted onClick={this.closeCloseModal}>
              <Icon name='remove' /> No
            </Button>
            <Button color='green' inverted onClick={this.closeCloseModal}>
              <Icon name='checkmark' /> Yes
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    )
  }
}