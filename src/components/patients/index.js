import React, { Component } from 'react';
import Layout from './../layout/layout';
import { Grid, Table, Dropdown, Modal, Icon, Button, Form, Header, Image } from 'semantic-ui-react';
import TableLoader from './../loaders/table';

import avatar from './../../assets/img/avatar.png';

import * as patientsActions from './../../store/actions/patientsActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Moment from 'react-moment';

const gender = [
    { key: 'm', text: 'Male', value: 'male' },
    { key: 'f', text: 'Female', value: 'female' },
];

const hospital = [
    { key: 1, text: 'Aga Khan Hospital', value: 'Aga Khan Hospital' },
    { key: 2, text: 'Nairobi Hospital', value: 'Nairobi Hospital' },
    { key: 3, text: 'Kenyatta Hospital', value: 'Kenyatta Hospital' },
    { key: 4, text: 'Mama Lucy Hospital', value: 'Mama Lucy Hospital' },
];

class Patients extends Component {

state = {
    loading: true,
    openAddModal: false,
    sizeAddModal: 'large',
}

// Add Patient Modal Window Functions
showAddModal = size => () => this.setState({ sizeAddModal: size, openAddModal: true })
closeAddModal = () => this.setState({ openAddModal: false })

change = () => {

}

componentDidMount() {
    this.props.actions.loadAllPatients();
    setTimeout(() => this.setState({ loading: false }), 1500); // simulates an async action, and hides the spinner
}

  render() {

    const {loading, sizeAddModal, openAddModal} = this.state;
    const {patients} = this.props;
    const patients_list = [];
    patients.map((patient, i) => {
        return patients_list.push(
            <Table.Row key={i}>
                <Table.Cell><Moment format="YYYY/MM/DD">{patient.created_at}</Moment></Table.Cell>
                <Table.Cell><Moment format="HH:mm:ss A">{patient.created_at}</Moment></Table.Cell>
                <Table.Cell>{patient.firstname} {patient.middlename} {patient.lastname}</Table.Cell>
                <Table.Cell>{patient.email}</Table.Cell>
                <Table.Cell>{patient.patient_no}</Table.Cell>
                <Table.Cell>{patient.gender}</Table.Cell>
                <Table.Cell><Moment format="YYYY/MM/DD">{patient.birth_date}</Moment></Table.Cell>
                <Table.Cell>{patient.hospital["name"]}</Table.Cell>
                <Table.Cell>
                    <Dropdown text='Actions'>
                        <Dropdown.Menu>
                            {/* Edit Patient Details Modal Window */}
                            <Modal size="large" trigger={ <Dropdown.Item text='Edit'/>} closeIcon>
                                <Modal.Header>Edit {patient.firstname} {patient.middlename} {patient.lastname}'s Details</Modal.Header>
                                <Modal.Content>
                                    <Form>
                                        <Form.Group widths='equal'>
                                            <Form.Input fluid label='Firstname' placeholder='Firstname' value={patient.firstname} onChange={this.change()}/>
                                            <Form.Input fluid label='Middlename' placeholder='Middlename' value={patient.middlename}  onChange={this.change}/>
                                            <Form.Input fluid label='Lastname' placeholder='Lastname' value={patient.lastname}  onChange={this.change}/>
                                        </Form.Group>
                                        <Form.Group widths='equal'>
                                            <Form.Input fluid label='Email Address' placeholder='Email Address' value={patient.email}  onChange={this.change}/>
                                            <Form.Field label='Patient Phone Number' control='input' type='number' placeholder='Phone Number' value={patient.patient_no}  onChange={this.change}/>
                                        </Form.Group>
                                        <Form.Group widths='equal'>
                                            <Form.Field label='Date Of Birth' control='input' type='date' value={patient.birth_date}  onChange={this.change}/>
                                            <Form.Select fluid label='Gender' options={gender} placeholder='Gender' value={patient.gender}  onChange={this.change}/>
                                            <Form.Select fluid label='Hospital' options={hospital} placeholder='Hospital'  onChange={this.change}/>
                                        </Form.Group>
                                    </Form>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button color='red'>
                                    <Icon name='remove' /> Cancel
                                    </Button>
                                    <Button primary>
                                        <Icon name='edit' /> Update Details
                                    </Button>
                                </Modal.Actions>
                </Modal>
                            {/* View Patient Details Modal Window */}
                            <Modal size="large" trigger={<Dropdown.Item text='View'/>} closeIcon>
                                <Modal.Header>
                                    <Header as='h2'>
                                        <Image circular src={avatar} /> 
                                        <Header.Content className="blue">
                                            {patient.firstname} {patient.middlename} {patient.lastname}'s Details
                                            <Header.Subheader>Created On: <Moment format="YYYY/MM/DD">{patient.created_at}</Moment> <Moment format="HH:mm:ss A">{patient.created_at}</Moment></Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                </Modal.Header>
                                <Modal.Content scrolling>
                                    <Modal.Description>
                                        <Grid stackable>
                                            <Grid.Row columns={1}>
                                                <Grid.Column>
                                                    <Table basic='very'>
                                                        <Table.Body>
                                                            <Table.Row>
                                                                <Table.HeaderCell>Name(s)</Table.HeaderCell>
                                                                <Table.Cell>{patient.firstname} {patient.middlename} {patient.lastname}</Table.Cell>
                                                                <Table.HeaderCell>Email Address</Table.HeaderCell>
                                                                <Table.Cell>{patient.email}</Table.Cell>
                                                            </Table.Row>
                                                            <Table.Row>
                                                                <Table.HeaderCell>Date Of Birth</Table.HeaderCell>
                                                                <Table.Cell><Moment format="YYYY/MM/DD">{patient.birth_date}</Moment></Table.Cell>
                                                                <Table.HeaderCell>Phone Number</Table.HeaderCell>
                                                                <Table.Cell>{patient.patient_no}</Table.Cell>
                                                            </Table.Row>
                                                            <Table.Row>
                                                                <Table.HeaderCell>Hospital</Table.HeaderCell>
                                                                <Table.Cell>{patient.hospital["name"]}</Table.Cell>
                                                                <Table.HeaderCell>Gender</Table.HeaderCell>
                                                                <Table.Cell>{patient.gender}</Table.Cell>
                                                            </Table.Row>
                                                        </Table.Body>
                                                    </Table>
                                                </Grid.Column>
                                            </Grid.Row>
                                            {patient.procedures.length > 0 &&
                                            <Grid.Row columns={1}>
                                                <Grid.Column>
                                                    <div className="modal-heading-wrapper">
                                                        <h3 className="blue">{patient.firstname} {patient.middlename} {patient.lastname}'s Procedures</h3>
                                                    </div>
                                                </Grid.Column>
                                                <Grid.Column>
                                                    <Table basic>
                                                        <Table.Header>
                                                            <Table.Row>
                                                                <Table.HeaderCell>Date</Table.HeaderCell>
                                                                <Table.HeaderCell>Name</Table.HeaderCell>
                                                                <Table.HeaderCell>Description</Table.HeaderCell>
                                                            </Table.Row>
                                                        </Table.Header>
                                                        <Table.Body>

                                                                {patient.procedures.map((procedure, i) => (
                                                                    <Table.Row key={i}>
                                                                        <Table.Cell><Moment format="YYYY/MM/DD">{procedure.created_at}</Moment></Table.Cell>
                                                                        <Table.Cell>{procedure.name}</Table.Cell>
                                                                        <Table.Cell>{procedure.description}</Table.Cell>
                                                                    </Table.Row>
                                                                ))}
                                                            
                                                        </Table.Body>
                                                    </Table>
                                                </Grid.Column>
                                            </Grid.Row>}
                                        </Grid>
                                    </Modal.Description>
                                </Modal.Content>
                            </Modal>
                            <Dropdown.Divider />
                            {/* Delete Patient Modal Window */}
                            <Modal basic size="small" trigger={<Dropdown.Item text='Delete'/>} closeIcon>
                                <Header icon='trash alternate' content='Delete Patient Details' />
                                <Modal.Content>
                                    <p>
                                        Are You Sure You Want To Delete {patient.firstname} {patient.middlename} {patient.lastname}'s Details ?
                                    </p>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button color='red' inverted>
                                        <Icon name='remove' /> No
                                    </Button>
                                    <Button color='green' inverted>
                                        <Icon name='checkmark' /> Yes
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        </Dropdown.Menu>
                    </Dropdown>
                </Table.Cell>
            </Table.Row>
        );
    })

    return (
        <Layout>
            <div className="page-wrapper">
                <Grid stackable>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <div className="flex-box">
                                <div>
                                    {loading && <TableLoader width="103px" height="36px"/>}
                                    {!loading &&
                                    <h1>Patients</h1>
                                    }
                                </div>
                                <div>
                                    {loading && <TableLoader width="138px" height="36px"/>}
                                    {!loading &&
                                    <Button color='green' className="btn" onClick={this.showAddModal('large')}>
                                        <Icon name='plus' /> Add Patient
                                    </Button>
                                    }
                                </div>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            {loading && <TableLoader width="100%" height="41px" loop={6}/>}
                            {!loading && 
                            <Table basic='very'>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>Date</Table.HeaderCell>
                                        <Table.HeaderCell>Time</Table.HeaderCell>
                                        <Table.HeaderCell>Name</Table.HeaderCell>
                                        <Table.HeaderCell>E-mail address</Table.HeaderCell>
                                        <Table.HeaderCell>Phone No.</Table.HeaderCell>
                                        <Table.HeaderCell>Gender</Table.HeaderCell>
                                        <Table.HeaderCell>Birth Date</Table.HeaderCell>
                                        <Table.HeaderCell>Hospital</Table.HeaderCell>
                                        <Table.HeaderCell>Action</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {patients_list}
                                </Table.Body>
                            </Table>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                {/* Add Patient Details Modal Window */}
                <Modal size={sizeAddModal} open={openAddModal} onClose={this.closeAddModal} closeIcon>
                    <Modal.Header>Add Patient</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input fluid label='Firstname' placeholder='Firstname' />
                                <Form.Input fluid label='Middlename' placeholder='Middlename' />
                                <Form.Input fluid label='Lastname' placeholder='Lastname' />
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Input fluid label='Email Address' placeholder='Email Address' />
                                <Form.Field label='Patient Phone Number' control='input' type='number' placeholder='Phone Number' />
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Field label='Date Of Birth' control='input' type='date' />
                                <Form.Select fluid label='Gender' options={gender} placeholder='Gender' />
                                <Form.Select fluid label='Hospital' options={hospital} placeholder='Hospital' />
                            </Form.Group>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='red' onClick={this.closeAddModal}>
                        <Icon name='remove' /> Cancel
                        </Button>
                        <Button color='green' className="btn" onClick={this.closeAddModal}>
                            <Icon name='save' /> Add Patient Details
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        </Layout>
    )
  }
}

Patients.propTypes = {
    dispatch: PropTypes.func,
    actions: PropTypes.object.isRequired
  }
  
  const mapStateToProps = (state, ownProps) => {
    return {
      patients: state.patients
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(patientsActions, dispatch)
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Patients);
