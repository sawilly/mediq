import React, { Component } from 'react';
import Layout from './../layout/layout';
import { Grid, Table, Dropdown, Modal, Icon, Button, Form, Header } from 'semantic-ui-react';
import TableLoader from './../loaders/table';

const procedures = [
  { key: 1, text: 'Dental Alignment', value: 1 },
  { key: 2, text: 'Malaria Treatment', value: 2 },
  { key: 3, text: 'Eye Surgery', value: 3 },
  { key: 4, text: 'Physiotherapy', value: 4 },
];

const options = [
  { key: 1, text: 'Insurance', value: 'insurance' },
  { key: 2, text: 'Hospital', value: 'hospital' },
  { key: 3, text: 'Patient', value: 'patient' }
];

export default class billings extends Component {

   //Initial States For Procedure Component
   state = {
    loading: true,
    open: false, 
    size: 'large',
    openEditModal: false,
    sizeEditModal: 'large',
    openDeleteModal: false,
    sizeDeleteModal: 'small'
  }

  // Add Medical Note Modal Window Functions
  show = size => () => this.setState({ size, open: true })
  close = () => this.setState({ open: false })

  // Edit Medical Note Modal Window Functions
  showEditModal = size => () => this.setState({ sizeEditModal: size, openEditModal: true })
  closeEditModal = () => this.setState({ openEditModal: false })

  // Delete Medical Note Modal Window Functions
  showDeleteModal = size => () => this.setState({ sizeDeleteModal: size, openDeleteModal: true })
  closeDeleteModal = () => this.setState({ openDeleteModal: false })


  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 1500); // simulates an async action, and hides the spinner
  }

  render() {

    const {size, open, loading, openEditModal, sizeEditModal, openDeleteModal, sizeDeleteModal} = this.state;

    return (
      <div>
        <Layout>
            <div className="page-wrapper">
              <Grid stackable>
                <Grid.Row columns={1}>
                  <Grid.Column>
                    <div className="flex-box">
                      <div>
                        {loading && <TableLoader height="36px" width="91px"/>}
                        {!loading && 
                        <h1>Billings</h1>
                        }
                      </div>
                      <div>
                        {loading && <TableLoader height="36px" width="132px"/>}
                        {!loading && 
                        <Button color='green' className="btn" onClick={this.show('large')}>
                          <Icon name='plus' /> Add Billing
                        </Button>
                        }
                      </div>
                    </div>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={1}>
                  <Grid.Column>
                    {loading && <TableLoader width="100%" height="41px" loop={5}/>}
                    {!loading && 
                    <Table basic='very'>
                        <Table.Header>
                            <Table.Row>
                              <Table.HeaderCell>Date</Table.HeaderCell>
                              <Table.HeaderCell>Procedure</Table.HeaderCell>
                              <Table.HeaderCell>Total Procedure Cost</Table.HeaderCell>
                              <Table.HeaderCell>Amount Recieved</Table.HeaderCell>
                              <Table.HeaderCell>Remaining Balance</Table.HeaderCell>
                              <Table.HeaderCell>Billed To</Table.HeaderCell>
                              <Table.HeaderCell>Patient</Table.HeaderCell>
                              <Table.HeaderCell>Action</Table.HeaderCell>
                            </Table.Row>
                          </Table.Header>
                          <Table.Body>
                            <Table.Row>
                              <Table.Cell>September 14, 2013</Table.Cell>
                              <Table.Cell>Tenetur</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell positive>0.00</Table.Cell>
                              <Table.Cell negative>0.00</Table.Cell>
                              <Table.Cell>Insurance</Table.Cell>
                              <Table.Cell>John Lilki</Table.Cell>
                              <Table.Cell>
                                <Dropdown text='Actions'>
                                  <Dropdown.Menu>
                                      <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                      <Dropdown.Item text='View' />
                                      <Dropdown.Divider />
                                      <Dropdown.Item text='Delete' onClick={this.showDeleteModal('small')}/>
                                    </Dropdown.Menu>
                                </Dropdown>
                              </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                              <Table.Cell>September 14, 2013</Table.Cell>
                              <Table.Cell>Tenetur</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>Insurance</Table.Cell>
                              <Table.Cell>John Lilki</Table.Cell>
                              <Table.Cell>
                                <Dropdown text='Actions'>
                                  <Dropdown.Menu>
                                      <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                      <Dropdown.Item text='View' />
                                      <Dropdown.Divider />
                                      <Dropdown.Item text='Delete' onClick={this.showDeleteModal('small')}/>
                                    </Dropdown.Menu>
                                </Dropdown>
                              </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                              <Table.Cell>September 14, 2013</Table.Cell>
                              <Table.Cell>Tenetur</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>Insurance</Table.Cell>
                              <Table.Cell>John Lilki</Table.Cell>
                              <Table.Cell>
                                <Dropdown text='Actions'>
                                  <Dropdown.Menu>
                                      <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                      <Dropdown.Item text='View' />
                                      <Dropdown.Divider />
                                      <Dropdown.Item text='Delete' onClick={this.showDeleteModal('small')}/>
                                    </Dropdown.Menu>
                                </Dropdown>
                              </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                              <Table.Cell>September 14, 2013</Table.Cell>
                              <Table.Cell>Tenetur</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>0.00</Table.Cell>
                              <Table.Cell>Insurance</Table.Cell>
                              <Table.Cell>John Lilki</Table.Cell>
                              <Table.Cell>
                                <Dropdown text='Actions'>
                                  <Dropdown.Menu>
                                      <Dropdown.Item text='Edit' onClick={this.showEditModal('large')}/>
                                      <Dropdown.Item text='View' />
                                      <Dropdown.Divider />
                                      <Dropdown.Item text='Delete' onClick={this.showDeleteModal('small')}/>
                                    </Dropdown.Menu>
                                </Dropdown>
                              </Table.Cell>
                            </Table.Row>
                          </Table.Body>
                      </Table>
                      }
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              {/* Modal Window For Adding Medical Note */}
              <Modal size={size} open={open} onClose={this.close} closeIcon>
                <Modal.Header>Add Billing Record</Modal.Header>
                  <Modal.Content>
                    <Form>
                      <Form.Group widths='equal'>
                          <Form.Select fluid label='Procedure' options={procedures} placeholder='Procedure' />
                        </Form.Group>
                        <Form.Group widths='equal'>
                          <Form.Select fluid label='Billed To' options={options} placeholder='Billed To' />
                        </Form.Group>
                        <Form.Group widths='equal'>
                          <Form.Field label='Total Procedure Cost' control='input' type='number' placeholder='Total Procedure Cost' />
                        </Form.Group>
                        <Form.Group widths='equal'>
                          <Form.Field label='Amount Received' control='input' type='number' placeholder='Amount Received' />
                        </Form.Group>
                    </Form>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button color='red' onClick={this.close}>
                      <Icon name='remove' /> Cancel
                    </Button>
                      <Button color='green' className="btn" onClick={this.close}>
                        <Icon name='save' /> Add Billing Record
                      </Button>
                  </Modal.Actions>
              </Modal>
              {/* Modal Window For Editing Medical Note */}
              <Modal size={sizeEditModal} open={openEditModal} onClose={this.closeEditModal} closeIcon>
                <Modal.Header>Edit Billing Record</Modal.Header>
                  <Modal.Content>
                    <Form>
                      <Form.Group widths='equal'>
                        <Form.Select fluid label='Procedure' options={procedures} placeholder='Procedure' />
                      </Form.Group>
                      <Form.Group widths='equal'>
                        <Form.Select fluid label='Billed To' options={options} placeholder='Billed To' />
                      </Form.Group>
                      <Form.Group widths='equal'>
                        <Form.Field label='Total Procedure Cost' control='input' type='number' placeholder='Total Procedure Cost' />
                      </Form.Group>
                      <Form.Group widths='equal'>
                        <Form.Field label='Amount Received' control='input' type='number' placeholder='Amount Received' />
                      </Form.Group>
                    </Form>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button color='red' onClick={this.closeEditModal}>
                      <Icon name='remove' /> Cancel
                    </Button>
                      <Button primary onClick={this.closeEditModal}>
                        <Icon name='edit' /> Edit Billing Record
                      </Button>
                  </Modal.Actions>
              </Modal>
                {/* Delete Medical Note Modal Window */}
                <Modal basic size={sizeDeleteModal} open={openDeleteModal} onClose={this.closeDeleteModal} closeIcon>
                    <Header icon='trash alternate' content='Delete Billing Record' />
                    <Modal.Content>
                        <p>
                            Are You Sure You Want To Delete The Billing Record ?
                        </p>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='red' inverted onClick={this.closeDeleteModal}>
                            <Icon name='remove' /> No
                        </Button>
                        <Button color='green' inverted onClick={this.closeDeleteModal}>
                            <Icon name='checkmark' /> Yes
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        </Layout>
      </div>
    )
  }
}