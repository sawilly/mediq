import React, { Component } from 'react';
import { Link, Redirect} from 'react-router-dom';
import {
  Container,
  Grid,
  List,
  Menu,
  Segment,
  Button,
  Icon,
  Popup,
  Modal,
  Form,
  Message
} from 'semantic-ui-react';
import './../App.css';
import Background from './../assets/img/6.jpg';
import LazyHero from 'react-lazy-hero';
import {connect} from 'react-redux';
import * as authActions from './../store/actions/authActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';

const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
];

class Home extends Component {
  state = { 
    open: false, 
    signup: false, 
    login: [], 
    loading: false,

    //Login Form States
    username: '',
    usernameError: false, 
    password: '',
    passwordError: false,
    errors: [],
    error: false
  };

  //Assigning Input Values To States
  handleUserInput = e => {
    this.setState({
      [e.target.name]: e.target.value,
      error: false,
      [e.target.name + "Error"]: false
    })
  }

  validate = () => {
    let isError = false;
    const errors = [];

    if(this.state.username === '') {

      isError = true;
      errors.push("Email Address Is Required");
      this.setState({usernameError: true});

    } else if(this.state.username.indexOf('@') === -1) {

      isError = true;
      errors.push("Email Address Is Invalid");
      this.setState({usernameError: true});

    }

    if(this.state.password === '') {
      isError = true;
      errors.push("Password Is Required");
      this.setState({passwordError: true});
    }

    if(isError) {
      this.setState({
        errors: errors
      })
    }

    return isError;
  }

  //Sign In Modal Window Functions
  show = size => () => this.setState({ size, open: true })
  close = () => this.setState({ open: false })

  //Sign Up Modal Window Functions
  showSignUp = size => () => this.setState({ size, signup: true })
  closeSignUp = () => this.setState({ signup: false })

  //Authentication Function
  login = () => {
    const err = this.validate();
    if(!err){
      this.setState({ loading: true });
      const credentials = {
        username: this.state.username,
        password: this.state.password
      };
      this.props.actions.signin(credentials).then(() => {
        this.setState({ loading: false, open: false });
        window.location = "/dashboard";
      }).catch((error) => {
        this.setState({ loading: false, error: true, errors: [] });
        const errs = this.state.errors;
        errs.push(error)
        this.setState({ errors: errs });
      });
    } else{
      this.setState({error: true});
    }
    
  };

  render() {
    
    const list = [];
    const { open, size, signup, loading, username, password, usernameError, passwordError, error, errors } = this.state;
    const { from } = this.props.location.state || { from: { pathname: '/dashboard'}};

    if(this.props.auth === true) {
      return (
        <Redirect to={from} />
      )
    }

    if(errors) {
      errors.map(function(error, i) {
        return (
          list.push(<li key={i}>{error}</li>)
        )
     })
    }

    return (
      <div>
        <Menu fixed='top' borderless className="menu">
          <Container>
            <Menu.Item as={Link} to="/" header className="menu-header">
              <span className="green">MEDIQ</span><span className="blue">BILL</span>
            </Menu.Item>
            <Menu.Menu position='right'>
              <Menu.Item as={Link} to="/" name="Home" active={true} className="menu-item">Home</Menu.Item>
              <Menu.Item name="SignIn" className="menu-item auth-links" onClick={this.show('small')}>Sign In</Menu.Item>
              <Menu.Item name="SignUp" className="menu-item auth-links" onClick={this.showSignUp('large')}>Sign Up</Menu.Item>
            </Menu.Menu>
          </Container>
        </Menu>
        <section>
        <LazyHero imageSrc={Background} color="#000000" opacity={0.8} parallaxOffset={10} className="parallax-wrapper" minHeight="768px">
          <Container>
            <div className="parallax-content">
                <h1 className="logo"><span className="green">MEDIQ</span><span className="blue">BILL</span></h1>
                <h2 className="alert">Join Us Today !</h2>
                <p className="sub-heading">Simple Way to Track Your Personal Online Record of Everyday Patient Billing.</p>
                <Popup
                  trigger={<Button color='green' animated className="banner-button" onClick={this.show('small')}>
                  <Button.Content visible>Sign In</Button.Content>
                  <Button.Content hidden>
                    <Icon name='sign in alternate' />
                  </Button.Content>
                </Button> }
                  content='Already Having An Account ? Sign In With Your Correct Credentials'
                  hideOnScroll
                />
                <Popup
                  trigger={<Button primary animated onClick={this.showSignUp('large')}>
                  <Button.Content visible>Sign Up</Button.Content>
                  <Button.Content hidden>
                    <Icon name='edit outline' />
                  </Button.Content>
                </Button> }
                  content='Join Us Today By Creating An Account With Us'
                  hideOnScroll
                />
            </div>
          </Container>
        </LazyHero>
        </section>
        <section style={{ height: '400px' }} className="content-section">
          <Container>
            <Grid container stackable columns={2}>
              <Grid.Row className="section">
                  <Grid.Column>
                    <h2 className="section-h2">Mediqbill</h2>
                    <p className="section-p">Mediqbill is a web application designed for specialist doctors who are in private practice to aid them keep a personal online track record of everyday patient billing and also to aid them in following up with specific payors.</p>
                  </Grid.Column>
                  <Grid.Column>
                    <h2 className="section-h2">Keep In Touch</h2>
                    <List>
                      <List.Item className="link">
                        <List.Icon name='users' />
                        <List.Content>Mediqbill</List.Content>
                      </List.Item>
                      <List.Item className="link">
                        <List.Icon name='marker' />
                        <List.Content>Nairobi, Kenya</List.Content>
                      </List.Item>
                      <List.Item className="link">
                        <List.Icon name='mail' />
                        <List.Content>
                          <a className="external-links" href='mailto:support@mediqbill.com'>support@mediqbill.com</a>
                        </List.Content>
                      </List.Item>
                      <List.Item className="link">
                        <List.Icon name='linkify' />
                        <List.Content>
                          <a className="external-links" href='http://mediqbill.com'>mediqbill.com</a>
                        </List.Content>
                      </List.Item>
                    </List>
                  </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </section>
        <Segment inverted vertical style={{ padding: '1em 0em' }} className="footer">
          <Container>
            <Grid divided inverted stackable>
              <Grid.Row>
                <h5>&copy; Powered By NectarineIT</h5>
              </Grid.Row>
            </Grid>
          </Container>
        </Segment>
        {/* Sign In Modal Window */}
        <Modal size={size} open={open} onClose={this.close} closeIcon>
          <Modal.Header>Log in to Mediqbill</Modal.Header>
          <Modal.Content>
            <Form loading={loading}>
              <Form.Group widths='equal'>
                <Form.Input error={usernameError} name="username" fluid label='Email Address' placeholder='Email Address' value={username} onChange={(event) => this.handleUserInput(event)}/>
                <Form.Input error={passwordError} name="password" fluid label='Password' placeholder='Password' value={password} onChange={(event) => this.handleUserInput(event)}/>
              </Form.Group>
            </Form>
            {error &&
              <Message error>
                <Message.Header>There was some error(s) with your submission</Message.Header>
                <ul>
                  {list}
                </ul>
              </Message>
            }
          </Modal.Content>
          <Modal.Actions>
            <Button color='red' onClick={this.close}>
              <Icon name='remove' /> Cancel
            </Button>
            <Button loading={loading} className="banner-button" onClick={this.login}>
              <Icon name='checkmark' /> Sign In
            </Button>
          </Modal.Actions>
        </Modal>
        {/* Sign Up Modal Window */}
        <Modal size={size} open={signup} onClose={this.closeSignUp} closeIcon>
          <Modal.Header>Create Mediqbill Account</Modal.Header>
          <Modal.Content>
            <Form>
              <Form.Group widths='equal'>
                <Form.Input fluid label='Firstname' placeholder='Firstname' />
                <Form.Input fluid label='Middlename' placeholder='Middlename' />
                <Form.Input fluid label='Lastname' placeholder='Lastname' />
                <Form.Input fluid label='Email Address' placeholder='Email Address' />
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Input fluid label='National ID No.' placeholder='National ID No.' />
                <Form.Input fluid label='Doctor Speciality' placeholder='Doctor Speciality' />
                <Form.Select fluid label='Gender' options={options} placeholder='Gender' />
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Input fluid label='Date Of Birth' placeholder='Date Of Birth' />
                <Form.Input fluid label='Board Reg No.' placeholder='Board Reg No.' />
                <Form.Input fluid label='Personal Number' placeholder='Personal Number' />
                <Form.Input fluid label='Office Number' placeholder='Office Number' />
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Input fluid label='Password' placeholder='Password' />
                <Form.Input fluid label='Confirm Password' placeholder='Confirm Password' />
              </Form.Group>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color='red' onClick={this.closeSignUp}>
              <Icon name='remove' /> Cancel
            </Button>
            <Button className="banner-button">
              <Icon name='pencil alternate' /> Sign Up
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(authActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
