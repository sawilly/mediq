import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { Button, Header, Icon, Menu, Segment, Sidebar, Container, Modal, Dropdown, Search } from 'semantic-ui-react';

import './../../assets/css/styles.css';

import avatar from './../../assets/img/avatar.png';

import background from './../../assets/img/5.jpg';

import {connect} from 'react-redux';

import * as authActions from './../../store/actions/authActions';

import PropTypes from 'prop-types';

import {bindActionCreators} from 'redux';

class Dashboard extends Component {

    state = { visible: false, signout: false, activeItem: '', loading: false }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    handleButtonClick = () => this.setState({ visible: !this.state.visible })
    handleSidebarHide = () => this.setState({ visible: false })

    //Logout Modal Window Functions
    logOutModal = () => this.setState({ signout: true })
    closeLogOut = () => this.setState({ signout: false })

    //Sigining Out From Mediqbill
    signout = () => {
        this.setState({ loading: true });
        this.props.actions.signout().then(()=> {
            this.setState({ loading: false, signout: false });
            window.location = "/";
        });
    };

    render() {

        const { visible, signout, activeItem, loading } = this.state;

        return (
            <div>
                <Sidebar.Pushable as={Segment} className="sidebar-wrapper">
                    <Sidebar
                        as={Menu}
                        animation='overlay'
                        icon='labeled'
                        inverted
                        vertical
                        visible={visible}
                        onHide={this.handleSidebarHide}
                        className="sidebar"
                        style={{backgroundImage: `url(${background})`}}
                        width="wide"
                    >
                        <div className="sidebar-menu-wrapper">
                        {/* <Menu.Item as='a'><span><Icon name='dashboard' /> Dashboard</span></Menu.Item> */}
                        <div className="wrapper">
                            <div className="profile-header-container">  
                                <div className="profile-header-img">
                                    <img className="img-circle" src={avatar} alt="avatar" />
                                
                                    <div className="rank-label-container">
                                        Dr. Kevin Moturi {this.props.auth ? 'True' : 'False'}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <Menu.Item as={Link} to="/dashboard" className="sidebar-menu-item" onClick={this.handleSidebarHide.bind(this)}><span><Icon className="menu-icon" name='dashboard' /> Dashboard</span></Menu.Item>
                        <Menu.Item as={Link} to="/patients" className="sidebar-menu-item" onClick={this.handleSidebarHide.bind(this)}><span><Icon className="menu-icon" name='users' /> Patients</span></Menu.Item>
                        <Menu.Item as={Link} to="/procedures" className="sidebar-menu-item" onClick={this.handleSidebarHide.bind(this)}><span><Icon className="menu-icon" name='user md' /> Procedures</span></Menu.Item>
                        <Menu.Item as={Link} to="/notes" className="sidebar-menu-item" onClick={this.handleSidebarHide.bind(this)}><span><Icon className="menu-icon" name='file alternate' /> Notes</span></Menu.Item>
                        <Menu.Item as={Link} to="/billings" className="sidebar-menu-item" onClick={this.handleSidebarHide.bind(this)}><span><Icon className="menu-icon" name='money bill alternate' /> Billings</span></Menu.Item>
                        {/* <Menu.Item as={Link} to="/profile" className="sidebar-menu-item"><span><Icon className="menu-icon" name='vcard' /> My Profile</span></Menu.Item>
                        <Menu.Item as={Link} to="/settings" className="sidebar-menu-item"><span><Icon className="menu-icon" name='setting' /> Settings</span></Menu.Item>
                        <Menu.Item className="sidebar-menu-item" onClick={this.logOutModal}><span><Icon className="menu-icon" name='log out' /> Sign Out</span></Menu.Item> */}
                        <Menu.Item className="sidebar-menu-item">
                            <Dropdown text='Account'>
                                <Dropdown.Menu>
                                    <Dropdown.Item as={Link} to="/profile"><Icon name='vcard' className="icon-right"/> My Profile</Dropdown.Item>
                                    <Dropdown.Item as={Link} to="/settings"><Icon name='setting' className="icon-right"/> Settings</Dropdown.Item>
                                    <Dropdown.Divider />
                                    <Dropdown.Item onClick={this.logOutModal}><Icon name='log out' className="icon-right"/> Log Out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu.Item>
                        </div>
                    </Sidebar>
                    <Sidebar.Pusher dimmed={visible}>
                        <section>
                            <Menu borderless fixed="top">
                                    <Menu.Item onClick={this.handleButtonClick} className="drawer-button">
                                        <span className="blue"><Icon name='align justify' /></span>
                                    </Menu.Item>
                                    <Menu.Item as={Link} to="/" header className="menu-header">
                                        <span className="green">MEDIQ</span><span className="blue">BILL</span>
                                    </Menu.Item>
                                    <Menu.Menu position='right' className="menu-right">
                                        <Menu.Item>
                                            <Search />
                                        </Menu.Item>
                                        <Menu.Item as={Link} to="/dashboard" name='dashboard' active={activeItem === 'dashboard'} onClick={this.handleItemClick}>
                                            <span>Dashboard</span>
                                        </Menu.Item>
                                        <Menu.Item as={Link} to="/patients" name='patients' active={activeItem === 'patients'} onClick={this.handleItemClick}>
                                            <span>Patients</span>
                                        </Menu.Item>
                                        <Menu.Item as={Link} to="/procedures" name='procedures' active={activeItem === 'procedures'} onClick={this.handleItemClick}>
                                            <span>Procedures</span>
                                        </Menu.Item>
                                        <Menu.Item as={Link} to="/notes" name='notes' active={activeItem === 'procedures'} onClick={this.handleItemClick}>
                                            <span>Notes</span>
                                        </Menu.Item>
                                        <Menu.Item as={Link} to="/billings" name='billings' active={activeItem === 'billings'} onClick={this.handleItemClick}>
                                            <span>Billings</span>
                                        </Menu.Item>
                                        <Menu.Item>
                                        <Dropdown text='Account'>
                                            <Dropdown.Menu>
                                                <Dropdown.Item as={Link} to="/profile" icon='vcard' text='My Profile' />
                                                <Dropdown.Item as={Link} to="/settings" icon='setting' text='Settings' />
                                                <Dropdown.Divider />
                                                <Dropdown.Item icon='log out' text='Sign Out' onClick={this.logOutModal}/>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                        </Menu.Item>
                                    </Menu.Menu>
                            </Menu>
                        </section>
                        <section style={{ minHeight: '100%', marginBottom: '8em' }}>
                            <Container>
                                <div className="content">
                                    <div className="view">
                                        {this.props.children}
                                    </div>
                                </div>
                            </Container>
                        </section>
                    </Sidebar.Pusher>
                </Sidebar.Pushable>

                {/* Logging Out Modal Window */}
                <Modal onClose={this.closeLogOut} basic size='small' open={signout} closeIcon>
                    <Header icon='log out' content='MEDIQBILL' />
                    <Modal.Content>
                    <p>
                        Thank You For Using Mediqbill. Are You Sure You Want To Sign Out ?
                    </p>
                    </Modal.Content>
                    <Modal.Actions>
                    <Button color='red' inverted onClick={this.closeLogOut}>
                        <Icon name='remove' /> No
                    </Button>
                    <Button loading={loading} className="btn" color='green' inverted onClick={this.signout}>
                        <Icon name='checkmark' /> Yes
                    </Button>
                    </Modal.Actions>
                </Modal>

            </div>
        );
    }

}

Dashboard.propTypes = {
  dispatch: PropTypes.func,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(authActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);