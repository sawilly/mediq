import React, { Component } from 'react';
import ContentLoader from 'react-content-loader';
import PropTypes from 'prop-types';


export default class Table extends Component {
  render(props) {
    const {height, width, loop} = this.props;
    let loaders = []
    for (let i = 0; i < loop; ++i) {
        loaders.push(
            <ContentLoader style={{ width: width ? width : '100%', height: height ? height : '100%' }} preserveAspectRatio="none" key={i}>
                <rect x="0" y="0" rx="0" ry="0" width="400" height="800" />
            </ContentLoader>
        )
    }
    return (
        <div>
            {loop &&
                loaders
            }

            {!loop &&
                <ContentLoader style={{ width: width ? width : '100%', height: height ? height : '100%' }} preserveAspectRatio="none">
                    <rect x="0" y="0" rx="0" ry="0" width="400" height="800" />
                </ContentLoader>
            }
        </div>
    )
  }
}

Table.propTypes = {
    height: PropTypes.string,
    width: PropTypes.string,
    loop: PropTypes.number
}

