import React, { Component } from 'react';
import ContentLoader from 'react-content-loader';
import PropTypes from 'prop-types';


export default class Block extends Component {
  render(props) {
    const {height, width} = this.props;
    return (
      <ContentLoader style={{ width: width ? width : '100%', height: height ? height : '100%' }} preserveAspectRatio="none">
        <rect x="0" y="0" rx="0" ry="0" width="400" height="800" />
      </ContentLoader>
    )
  }
}

Block.propTypes = {
    height: PropTypes.string,
    width: PropTypes.string
}

