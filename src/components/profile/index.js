import React, { Component } from 'react';
import Layout from './../layout/layout';
import { Grid, Button, Icon, List, Image, Form, Modal } from 'semantic-ui-react';
import avatar from './../../assets/img/avatar.png';
import TableLoader from './../loaders/table';

import * as userActions from './../../store/actions/userActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

const gender = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
];

class Profile extends Component {
  //Initial States For Procedure Component
  state = {
    loading: true,
    open: false, 
    size: 'large'
  }

  //Edit Profile Modal Window Functions
  show = size => () => this.setState({ size, open: true })
  close = () => this.setState({ open: false })

  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 1500); // simulates an async action, and hides the spinner
    this.props.actions.loadDetails();
  }

  render() {

    const {size, open, loading} = this.state;
    const {user} = this.props;

    return (
      <div>
        <Layout>
            <div className="page-wrapper">
              <Grid stackable>
                <Grid.Row columns={1}>
                  <Grid.Column width={5}>
                    {loading && <center><TableLoader height="150px" width="150px"/></center>}
                    {!loading && 
                    <Image src={avatar} size='small' centered />
                    }
                  </Grid.Column>
                  <Grid.Column width={11}>
                    <Grid.Row columns={1}>
                      <Grid.Column>
                        <div className="flex-box">
                          <div>
                            {loading && <TableLoader height="36px" width="130px"/>}
                            {!loading && 
                            <h1>My Profile</h1>
                            }
                          </div>
                          <div>
                            {loading && <TableLoader height="36px" width="134px"/>}
                            {!loading && 
                            <Button color='green' className="btn" onClick={this.show('large')}>
                              <Icon name='edit' /> Edit Profile
                            </Button>
                            }
                          </div>
                        </div>
                      </Grid.Column>
                      <Grid.Column>
                        {loading && <div className="user-details-list"><TableLoader height="52px" loop={6}/></div>}
                        {!loading && 
                        <List relaxed='very' divided className="user-details-list">
                          <List.Item>
                            <List.Icon name='address book' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Firstname: </List.Header>
                              <List.Description>{user.firstname ? user.firstname : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='id badge' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Middlename: </List.Header>
                              <List.Description>{user.middlename ? user.middlename : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='address card' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Lastname: </List.Header>
                              <List.Description>{user.lastname ? user.lastname : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='envelope' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Email Address: </List.Header>
                              <List.Description>{user.email ? user.email : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='clipboard' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">National ID NO: </List.Header>
                              <List.Description>{user.national_id ? user.national_id : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='user md' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Doctor's Area Of Specialization: </List.Header>
                              <List.Description>{user.doctor_speciality ? user.doctor_speciality : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='venus mars' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Gender: </List.Header>
                              <List.Description>{user.gender ? user.gender : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='calendar' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Birth Date: </List.Header>
                              <List.Description>{user.birth_date ? user.birth_date : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='address card outline' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Board Registration Number: </List.Header>
                              <List.Description>{user.board_no ? user.board_no : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='mobile' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Personal Mobile Number: </List.Header>
                              <List.Description>{user.personal_number ? user.personal_number : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                          <List.Item>
                            <List.Icon name='phone' size='large' verticalAlign='middle' />
                            <List.Content>
                              <List.Header className="user-details-list-header">Office Number: </List.Header>
                              <List.Description>{user.office_number ? user.office_number : 'Null'}</List.Description>
                            </List.Content>
                          </List.Item>
                        </List>
                        }
                      </Grid.Column>
                    </Grid.Row>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
                {/* Edit Patient Details Modal Window */}
                <Modal size={size} open={open} onClose={this.close} closeIcon>
                    <Modal.Header>Edit Profile</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input fluid label='Firstname' placeholder='Firstname' value={user.firstname} />
                                <Form.Input fluid label='Middlename' placeholder='Middlename' value={user.middlename} />
                                <Form.Input fluid label='Lastname' placeholder='Lastname' value={user.lastname} />
                                <Form.Input fluid label='Email Address' placeholder='Email Address' value={user.email} />
                            </Form.Group>
                            <Form.Group widths='equal'>
                              <Form.Field label='Date Of Birth' control='input' type='date' value={user.birth_date} />
                              <Form.Select fluid label='Gender' options={gender} placeholder='Gender' value={user.gender} />
                              <Form.Field label='National ID Number' control='input' type='number' placeholder='National ID Number' value={user.national_id} />
                            </Form.Group>
                            <Form.Group widths='equal'>
                              <Form.Input fluid label='Area Of Specialization' placeholder='Area Of Specialization' value={user.doctor_speciality} />
                              <Form.Field label='Board Registration Number' control='input' type='number' placeholder='Board Registration Number' value={user.board_no} />
                              <Form.Field label='Personal Number' control='input' type='number' placeholder='Personal Number' value={user.personal_number} />
                              <Form.Field label='Office Number' control='input' type='number' placeholder='Office Number' value={user.office_number} />
                            </Form.Group>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='red' onClick={this.close}>
                          <Icon name='remove' /> Cancel
                        </Button>
                        <Button primary onClick={this.close}>
                          <Icon name='edit' /> Update Details
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        </Layout>
      </div>
    )
  }
}

Profile.propTypes = {
  dispatch: PropTypes.func,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);