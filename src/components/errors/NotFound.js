import React, { Component } from 'react';
import Layout from './../layout/layout';

export default class NotFound extends Component {
  render() {
    return (
      <div>
        <Layout>
            <div className="page-wrapper">
                <h1 className="notfound">404 NOT FOUND</h1>
            </div>
        </Layout>
      </div>
    )
  }
}