import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import Route from  'react-router-dom/Route';
import Home from './components/index';
import Dashboard from './components/dashboard/dashboard';
import Settings from './components/profile/settings';
import Patients from './components/patients/index';
import Procedures from './components/procedures/index';
import Billings from './components/billings/index';
import Profile from './components/profile/index';
import Notes from './components/notes/index';
import NotFound from './components/errors/NotFound';

const isAuthenticated = localStorage.getItem('access_token') ? true : false;

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuthenticated === true ? <Component {...props} /> : <Redirect to={{
      pathname: '/',
      state: { from: props.location }
    }} />
  )} />
);

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path="/" exact strict component={Home} />
            {/* <Route path="/dashboard" exact strict component={Dashboard} /> */}
            <PrivateRoute path="/dashboard" component={Dashboard} />
            {/* <Route path="/patients" exact strict component={Patients} /> */}
            <PrivateRoute path="/patients" component={Patients} />
            {/* <Route path="/procedures" exact strict component={Procedures} /> */}
            <PrivateRoute path="/procedures" component={Procedures} />
            {/* <Route path="/notes" exact strict component={Notes} /> */}
            <PrivateRoute path="/notes" component={Notes} />
            {/* <Route path="/billings" exact strict component={Billings} /> */}
            <PrivateRoute path="/billings" component={Billings} />
            {/* <Route path="/profile" exact strict component={Profile} /> */}
            <PrivateRoute path="/profile" component={Profile} />
            {/* <Route path="/settings" exact strict component={Settings} /> */}
            <PrivateRoute path="/settings" component={Settings} />
            <Route component={ NotFound } />
          </Switch>
        </Router>
    );
  }
}

export default App;
